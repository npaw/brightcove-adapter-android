package com.brightcove.player.samples.ssai.basic;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.brightcove.player.edge.Catalog;
import com.brightcove.player.edge.VideoListener;
import com.brightcove.player.event.EventEmitter;
import com.brightcove.player.event.EventType;
import com.brightcove.player.model.DeliveryType;
import com.brightcove.player.model.Video;
import com.brightcove.player.network.HttpRequestConfig;
import com.brightcove.player.view.BrightcoveExoPlayerVideoView;
import com.brightcove.player.view.BrightcovePlayer;
import com.brightcove.ssai.SSAIComponent;
import com.npaw.youbora.lib6.YouboraLog;
import com.npaw.youbora.lib6.brightcove.BrightcoveAdapter;
import com.npaw.youbora.lib6.brightcove.BrightcoveAdsAdapter;
import com.npaw.youbora.lib6.plugin.Options;
import com.npaw.youbora.lib6.plugin.Plugin;


public class MainActivity extends BrightcovePlayer {

    private final String TAG = this.getClass().getSimpleName();
    @SuppressWarnings("FieldCanBeLocal")
    private final String AD_CONFIG_ID_QUERY_PARAM_KEY = "ad_config_id";
    @SuppressWarnings("FieldCanBeLocal")
    private final String AD_CONFIG_ID_QUERY_PARAM_VALUE = "ba5e4879-77f0-424b-8c98-706ae5ad7eec";
    private SSAIComponent plugin;
    private Button seek1Sec;
    private Button seek1SecBack;

    private Plugin youboraPlugin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // When extending the BrightcovePlayer, we must assign brightcoveVideoView before
        // entering the superclass.  This allows for some stock video player lifecycle
        // management.
        setContentView(R.layout.ssai_activity_main);

        brightcoveVideoView = (BrightcoveExoPlayerVideoView) findViewById(R.id.brightcove_video_view);
        YouboraLog.setDebugLevel(YouboraLog.Level.VERBOSE);

        Options options = new Options();
        options.setAccountCode("powerdev");
        youboraPlugin = new Plugin(options, getApplicationContext());
        youboraPlugin.setActivity(this);
        youboraPlugin.setAdapter(new BrightcoveAdapter(brightcoveVideoView));
        youboraPlugin.setAdsAdapter(new BrightcoveAdsAdapter(brightcoveVideoView));

        super.onCreate(savedInstanceState);


        seek1Sec = findViewById(R.id.seek1Sec);
        seek1Sec.setOnClickListener(v -> {
            brightcoveVideoView.seekTo(brightcoveVideoView.getCurrentPositionLong() + 1000L);
        });

        seek1SecBack = findViewById(R.id.seek1SecBack);
        seek1SecBack.setOnClickListener(v -> {
            brightcoveVideoView.seekTo(brightcoveVideoView.getCurrentPositionLong() - 1000L);
        });

        //// Comment one of these
        defaultPlayback();
        // specificURLPlayback();
    }

    private void specificURLPlayback() {

        // Setup the error event handler for the SSAI plugin.
        registerErrorEventHandler();
        plugin = new SSAIComponent(this, brightcoveVideoView);
        View view = findViewById(R.id.ad_frame);
        if (view instanceof ViewGroup) {
            // Set the companion ad container,
            plugin.addCompanionContainer((ViewGroup) view);
        }

        Video video = Video.createVideo("http://livesim.dashif.org/livesim/chunkdur_1/ato_1/testpic_2s/Manifest.mpd", DeliveryType.DASH);
        video.getProperties().put(Video.Fields.PUBLISHER_ID, R.string.sdk_demo_policy_key);
        brightcoveVideoView.add(video);
        brightcoveVideoView.start();
    }

    private void defaultPlayback() {

        final EventEmitter eventEmitter = brightcoveVideoView.getEventEmitter();

        Catalog catalog = new Catalog.Builder(eventEmitter, getString(R.string.sdk_demo_account))
                .setBaseURL(Catalog.DEFAULT_EDGE_BASE_URL)
                .setPolicy(getString(R.string.sdk_demo_policy_key))
                .build();

        // Setup the error event handler for the SSAI plugin.
        registerErrorEventHandler();
        plugin = new SSAIComponent(this, brightcoveVideoView);
        View view = findViewById(R.id.ad_frame);
        if (view instanceof ViewGroup) {
            // Set the companion ad container,
            plugin.addCompanionContainer((ViewGroup) view);
        }

        // Set the HttpRequestConfig with the Ad Config Id configured in
        // your https://studio.brightcove.com account.
        HttpRequestConfig httpRequestConfig = new HttpRequestConfig.Builder()
                .addQueryParameter(AD_CONFIG_ID_QUERY_PARAM_KEY, AD_CONFIG_ID_QUERY_PARAM_VALUE)
                .build();

        catalog.findVideoByID(getString(R.string.video_id), httpRequestConfig, new VideoListener() {
            @Override
            public void onVideo(Video video) {
                // The Video Sources will have a VMAP url which will be processed by the SSAI plugin,
                // If there is not a VMAP url, or if there are any requesting or parsing error,
                // an EventType.ERROR event will be emitted.
                plugin.processVideo(video);
            }
        });

        brightcoveVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                brightcoveVideoView.start();
            }
        });

        /*
        brightcoveVideoView.getEventEmitter().on(EventType.PLAY, new EventListener() {
            @Override
            public void processEvent(Event event) {
                youboraPlugin.pauseJoinDuration();
                // Blocking code asking user to replay
                youboraPlugin.resumeJoinDuration();
            }
        });
        */
    }

    private void registerErrorEventHandler() {
        // Handle the case where the ad data URL has not been supplied to the plugin.
        EventEmitter eventEmitter = brightcoveVideoView.getEventEmitter();
        eventEmitter.on(EventType.ERROR, event -> Log.e(TAG, event.getType()));
    }
}
