package com.npaw.youbora.lib6.brightcove

import com.brightcove.iabparser.vast.Linear
import com.brightcove.iabparser.vmap.VMAP
import com.brightcove.player.event.Event
import com.brightcove.player.event.EventListener
import com.brightcove.player.event.EventType
import com.brightcove.player.view.BaseVideoView
import com.brightcove.ssai.ad.AdPod
import com.google.ads.interactivemedia.v3.api.AdEvent
import com.npaw.youbora.lib6.Timer

import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.adapter.AdAdapter

class BrightcoveAdsAdapter(player: BaseVideoView) : AdAdapter<BaseVideoView>(player) {
    private var lastReportedAdId = super.getResource()
    private var lastReportedAdTitle = super.getTitle()
    private var lastReportedAdPlayhead = super.getPlayhead()
    private var lastReportedAdDuration = super.getDuration()
    private var lastReportedIsAdSkippable = super.getIsAdSkippable()
    private var numberOfAds: Int? = super.getExpectedAds()
    private var expectedPattern = HashMap<String, MutableList<Long>>()
    private var adServer = "Brightcove"
    private var savedPlayhead = 0.0

    private var listenerToken: Int = 0

    private val imaAvailable: Boolean
    private val linearAvailable: Boolean

    private var quartileTimer: Timer? = null
    private var isFirstQuartileFired = false
    private var isSecondQuartileFired = false

    init {
        // Check for OUX and IMA ads availability
        linearAvailable = try {
            com.brightcove.iabparser.vast.Linear::class != null
        }catch (e: NoClassDefFoundError){
            false
        }
        imaAvailable = try {
            com.google.ads.interactivemedia.v3.api.AdEvent::class != null
        }catch (e: NoClassDefFoundError){
            false
        }

        if (linearAvailable) {
            YouboraLog.debug("Linear ads available")
            adServer += "Linear"
        }

        if (imaAvailable) {
            YouboraLog.debug("IMA ads available")
            adServer += "IMA"
        }

        registerListeners()
    }

    override fun registerListeners() {
        super.registerListeners()

        val listener = EventListener { event ->

            when (event.type) {
                "adDataReady" -> {
                    val vmap = event.properties["vmapResponse"] as VMAP
                    var lastTimeOffset = ""

                    vmap.adBreakList.forEach { adBreak ->
                        if (adBreak.timeOffset != lastTimeOffset) {
                            when (adBreak.timeOffset) {
                                "start" -> {
                                    val list = mutableListOf<Long>()
                                    list.add(0)
                                    expectedPattern["pre"] = list
                                }

                                "end" -> {
                                    val list = mutableListOf<Long>()
                                    player?.let { list.add(it.durationLong) }
                                    expectedPattern["post"] = list
                                }

                                else -> {
                                    var list = mutableListOf<Long>()
                                    val split = adBreak.timeOffset.split(":")
                                    val minutes = split[1].toLong()
                                    val seconds = split[2].split(".")[0].toLong()
                                    val timeOffset = minutes * 60 + seconds

                                    if (expectedPattern["mid"] != null) {
                                        list = expectedPattern["mid"]!!
                                    }

                                    list.add(timeOffset)
                                    expectedPattern["mid"] = list
                                }
                            }

                            lastTimeOffset = adBreak.timeOffset
                        }
                    }
                }

                EventType.AD_STARTED -> {
                    lastReportedAdTitle = event.properties[Event.AD_TITLE] as String?
                    if(event.properties.containsKey("adEvent")){
                        try {
                            val adEvent = event.properties["adEvent"] as AdEvent?
                            lastReportedAdDuration = adEvent?.ad?.duration
                            lastReportedIsAdSkippable = adEvent?.ad?.isSkippable
                        } catch (e:NoClassDefFoundError) {
                            YouboraLog.warn("Error AdEvent cast - NOT IMA SDK")
                        } catch (e: Exception) {
                            YouboraLog.warn("Error AdEvent cast - Exception")
                        }
                    }
                    
                    if(event.properties.containsKey(Event.AD_ID))
                        lastReportedAdId = event.properties[Event.AD_ID] as String?
                    else if(event.properties.containsKey("adTagUrl"))
                        lastReportedAdId = event.properties["adTagUrl"] as String?

                    if (linearAvailable) {
                        val linearAd = event.properties["vastLinear"] as Linear?

                        if (linearAd != null)
                            lastReportedAdDuration = linearAd.durationAsPosition / 1000.0

                        if (getAdFlags().isAdBreakStarted) fireStart()

                    } else if (imaAvailable) {
                        fireStart()
                    }
                }

                EventType.AD_PROGRESS -> {
                    lastReportedAdPlayhead = (event.properties[Event.PROGRESS_BAR_PLAYHEAD_POSITION_LONG] as Long?)?.toDouble()?.let{ it/1000 }?:run { (event.getIntegerProperty(Event.PLAYHEAD_POSITION_LONG)
                            .toDouble() / 1000) - savedPlayhead }
                    fireJoin()
                }

                EventType.AD_PAUSED -> firePause()
                EventType.AD_RESUMED -> fireResume()
                EventType.AD_COMPLETED -> {
                    lastReportedAdPlayhead?.let { savedPlayhead = it }
                    fireStop()
                }
                EventType.AD_ERROR -> {
                    fireFatalError("Unknown error", null, null)
                }

                EventType.BUFFERING_STARTED -> fireBufferBegin()
                EventType.BUFFERING_COMPLETED -> fireBufferEnd()
                EventType.AD_BREAK_COMPLETED -> {
                    YouboraLog.debug("Ads.EventType.AD_BREAK_COMPLETED")
                    fireAdBreakStop()
                }
                EventType.AD_BREAK_STARTED -> {
                    YouboraLog.debug("Ads.EventType.AD_BREAK_STARTED")
                    try {
                        val adPod = event.properties["adPod"] as AdPod?
                        numberOfAds = adPod?.adBreakList?.size
                    } catch (e: NoClassDefFoundError) {
                        YouboraLog.debug("Brightcove/ssai not included in this compilation. GivenAds is a limitation.")
                    }
                    fireStart()
                }
            }
        }

        player?.eventEmitter?.on(EventType.ANY, listener)?.let { listenerToken = it }
        quartileTimer = createQuartileTimer()
    }

    override fun unregisterListeners() {
        super.unregisterListeners()
        player?.eventEmitter?.off(EventType.ANY, listenerToken)
        quartileTimer?.stop()
        quartileTimer = null
    }

    override fun fireStart(params: MutableMap<String, String>) {
        getDuration()?.takeIf { it > 0 }.let {
            quartileTimer?.start()
        }
        super.fireStart(params)
    }

    private fun createQuartileTimer(): Timer {
        return Timer(object : Timer.TimerEventListener {
            override fun onTimerEvent(delta: Long) {
                getDuration()?.let { duration ->
                    getPlayhead()?.let { playhead ->
                        if (playhead > duration * 0.25 && !isFirstQuartileFired) {
                            fireQuartile(1)
                            isFirstQuartileFired = true
                        } else if (playhead > duration * 0.5 && !isSecondQuartileFired) {
                            fireQuartile(2)
                            isSecondQuartileFired = true
                        } else if (playhead > duration * 0.75) {
                            fireQuartile(3)
                            quartileTimer?.stop()
                        }
                    }
                }
            }
        }, 100)
    }

    private fun classExists(className: String): Boolean {
        return try {
            Class.forName(className, false, javaClass.classLoader)
            true
        } catch (e: Exception) {
            false
        }
    }

    private fun resetValues() {
        lastReportedAdId = super.getResource()
        lastReportedAdTitle = super.getTitle()
        lastReportedAdPlayhead = super.getPlayhead()
        lastReportedAdDuration = super.getDuration()
        lastReportedIsAdSkippable = super.getIsAdSkippable()
        isFirstQuartileFired = false
        isSecondQuartileFired = false
    }

    override fun getGivenBreaks(): Int {
        var breakCount = 0

        if (expectedPattern["pre"] != null) breakCount++
        if (expectedPattern["post"] != null) breakCount++
        expectedPattern["mid"]?.let { breakCount += it.size }

        return breakCount
    }

    override fun getBreaksTime(): MutableList<Long> {
        val list = mutableListOf<Long>()

        expectedPattern["pre"]?.let { list.add(it[0]) }
        expectedPattern["mid"]?.let {
            it.forEach { playhead -> list.add(playhead) }
        }

        list.sort()

        expectedPattern["post"]?.let { list.add(it[0]) }

        return list
    }

    override fun getGivenAds(): Int? { return numberOfAds }
    override fun getPlayerName(): String { return "Brightcove" }
    override fun getTitle(): String? { return lastReportedAdTitle }
    override fun getResource(): String? { return lastReportedAdId }
    override fun getPlayhead(): Double? { return lastReportedAdPlayhead }
    override fun getDuration(): Double? { return lastReportedAdDuration }
    override fun getIsAdSkippable(): Boolean? { return lastReportedIsAdSkippable }
    override fun getVersion(): String { return BuildConfig.VERSION_NAME + "-" + getPlayerName() }

    override fun fireStop(params: MutableMap<String, String>) {
        super.fireStop(params)
        resetValues()
    }

    override fun fireAdBreakStop(params: MutableMap<String, String>) {
        super.fireAdBreakStop(params)
        savedPlayhead = 0.0
    }
}
