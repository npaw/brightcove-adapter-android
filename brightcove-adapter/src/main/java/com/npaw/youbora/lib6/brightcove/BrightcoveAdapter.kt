package com.npaw.youbora.lib6.brightcove


import com.brightcove.player.event.Event
import com.brightcove.player.event.EventListener
import com.brightcove.player.event.EventType
import com.brightcove.player.model.Source
import com.brightcove.player.model.Video
import com.brightcove.player.util.StringUtil
import com.brightcove.player.view.BaseVideoView
import com.google.android.exoplayer2.ExoPlaybackException
import com.google.android.exoplayer2.source.BehindLiveWindowException
import com.google.android.exoplayer2.upstream.HttpDataSource
import com.npaw.youbora.lib6.YouboraLog
import com.npaw.youbora.lib6.YouboraUtil
import com.npaw.youbora.lib6.adapter.PlayerAdapter
import java.util.*

import kotlin.concurrent.schedule

open class BrightcoveAdapter(player: BaseVideoView) : PlayerAdapter<BaseVideoView>(player) {

    private var lastReportedVersion: String? = null
    private var lastReportedResource = super.getResource()
    private var lastReportedBitrate = super.getBitrate()
    private var lastReportedTitle = super.getTitle()
    private var lastReportedRendition = super.getRendition()
    private var lastReportedPlayhead = super.getPlayhead()

    private var ignoredErrors: MutableList<String>? = null
    private var fatalErrors: MutableList<String>? = null

    private var listenerToken: Int = 0
    private var listenerErrorToken: Int = 0

    private var lastErrorCode = "-1"

    private var bufferTimer: Timer? = null

    private var isAdBreak = false

    companion object { private const val NULL_STRING = "null" }

    init {
        registerListeners()
    }

    override fun registerListeners() {
        super.registerListeners()

        val eventEmitter = player?.eventEmitter
        val eventListener = EventListener { event ->
            when (event.type) {
                EventType.ANALYTICS_VIDEO_ENGAGEMENT -> {
                    val properties = event.properties

                    // Build rendition string
                    val bps = if (properties.containsKey(Event.RENDITION_INDICATED_BPS))
                        properties[Event.RENDITION_INDICATED_BPS] as Int
                    else
                        0

                    val w = if (properties.containsKey(Event.RENDITION_WIDTH))
                        properties[Event.RENDITION_WIDTH] as Int
                    else
                        0

                    val h = if (properties.containsKey(Event.RENDITION_HEIGHT))
                        properties[Event.RENDITION_HEIGHT] as Int
                    else
                        0

                    if (bps > 0 || (w > 0 && h > 0)) {
                        lastReportedRendition = YouboraUtil
                                .buildRenditionString(w, h, bps.toDouble())
                    }
                    if (properties.containsKey(Event.MEASURED_BPS))
                        lastReportedBitrate = properties[Event.MEASURED_BPS] as Long
                }

                EventType.VERSION -> {
                    lastReportedVersion = event.properties[Event.BUILD_VERSION] as String?
                }

                EventType.PLAY -> {
                    YouboraLog.debug("EventType.PLAY")
                    if (!isAdBreak) {
                        getVideoTitle(event)
                        fireResume()
                        fireStart()
                    }
                }

                EventType.AD_BREAK_STARTED -> isAdBreak = true
                EventType.AD_BREAK_COMPLETED -> isAdBreak = false

                EventType.PROGRESS -> {
                    val source = event.properties[Event.SOURCE] as Source?
                    val bitrate = source?.bitRate?.toLong()
                    lastReportedResource = source?.url

                    if (bitrate != null && bitrate > 0) lastReportedBitrate = bitrate
                    getPlayhead()?.takeIf { it > 0.1 }?.let { fireJoin() }
                }

                EventType.COMPLETED -> fireStop()
                EventType.PAUSE -> if (!isAdBreak) firePause()
                EventType.DID_RESUME_CONTENT -> if (!isAdBreak) fireResume()
                EventType.SEEK_TO -> {
                    YouboraLog.debug("EventType.SEEK_TO")
                    cancelBufferIfNeeded()
                    fireSeekBegin()
                }
                EventType.DID_SEEK_TO -> {
                    YouboraLog.debug("EventType.DID_SEEK_TO")
                    fireSeekBegin(true)
                }
                EventType.ACTIVITY_PAUSED -> firePause()
                EventType.ACTIVITY_RESUMED -> fireResume()
                EventType.ACTIVITY_DESTROYED -> fireStop()
                EventType.SOURCE_NOT_FOUND -> {
                    YouboraLog.debug("EventType.SOURCE_NOT_FOUND")
                    reportError(event)
                }
                EventType.SOURCE_NOT_PLAYABLE -> {
                    YouboraLog.debug("EventType.SOURCE_NOT_PLAYABLE")
                    reportError(event)
                }
                EventType.WILL_CHANGE_VIDEO -> willChangeVideo(event)
                EventType.DID_SET_SOURCE -> {
                    val source = event.properties[Event.SOURCE] as Source?
                    lastReportedResource = source?.url
                    val bitrate = source?.bitRate?.toLong()

                    if (bitrate != null && bitrate > 0) lastReportedBitrate = bitrate

                    YouboraLog.debug("EventType.DID_SET_SOURCE")
                }
                EventType.BUFFERING_STARTED -> {
                    YouboraLog.debug("EventType.BUFFERING_STARTED")
                    if (!isAdBreak) {

                        /** Seek Events: SEEK_TO[0MS],  BUFFERING_STARTED[0-10MS], DID_SEEK_TO[20-50]MS, BUFFERING_COMPLETED[-MS] **/
                        if(flags.isSeeking)
                            fireBufferBegin()
                        else {
                            bufferTimer = createBufferTimer()
                            bufferTimer?.schedule(150L) { fireBufferBegin() }
                        }
                    }
                }
                EventType.BUFFERING_COMPLETED -> {
                    YouboraLog.debug("EventType.BUFFERING_COMPLETED")
                    cancelBufferIfNeeded()
                    fireSeekEnd()
                    fireBufferEnd()
                }
            }
        }

        val eventErrorListener = EventListener { event ->
            if (EventType.ERROR == event.type && !event.properties[Event.ERROR_MESSAGE].toString().startsWith("onLoadError")) {
                reportError(event)
            }
        }

        eventEmitter?.on(EventType.ANY, eventListener)?.let { listenerToken = it }

        // Since the player reports error multiple times we have to listen only ".once"
        eventEmitter?.once(EventType.ERROR, eventErrorListener)?.let { listenerErrorToken = it }
    }

    private fun cancelBufferIfNeeded() {
        bufferTimer?.cancel()
        bufferTimer = null
    }

    private fun createBufferTimer(): Timer {
        return Timer("BufferBeginTask", false)
    }

    open fun willChangeVideo(event: Event) {
        plugin?.fireStop()

        val nextVideo = event.properties[Event.NEXT_VIDEO] as Video?
        lastReportedTitle = nextVideo?.getStringProperty(Video.Fields.NAME)
        YouboraLog.debug("WILL_CHANGE_VIDEO")
    }

    private fun getVideoTitle(event: Event){
        val nextVideo = event.properties[Event.VIDEO] as Video?
        lastReportedTitle = nextVideo?.getStringProperty(Video.Fields.NAME)
    }

    override fun unregisterListeners() {
        cancelBufferIfNeeded()
        player?.eventEmitter?.off(EventType.ANY, listenerToken)
        player?.eventEmitter?.off(EventType.ERROR, listenerErrorToken)
        super.unregisterListeners()
    }

    override fun fireStart(params: MutableMap<String, String>) {
        super.fireStart(params)

        ignoredErrors = ArrayList(1)
        ignoredErrors?.add("204")

        fatalErrors = ArrayList(0)
        fatalErrors?.add("")
    }

    override fun fireStop(params: MutableMap<String, String>) {
        super.fireStop(params)
        resetValues()
    }

    private fun reportError(event: Event) {
        val source = event.properties[Event.SOURCE] as Source?
        lastReportedResource = source?.url

        var code = event.properties[Event.ERROR_CODE].toString()

        if (code == NULL_STRING) code = event.properties["statusCode"].toString()

        // Once we got the code, check if it's ignored
        ignoredErrors?.takeIf { it.contains(code) }?.let {
            YouboraLog.debug("Ignoring error: $code")
            return
        }

        var msg = event.properties[Event.ERROR_MESSAGE].toString()

        if (msg == NULL_STRING) msg = event.properties["message"].toString()

        var metadata = event.properties[Event.ERROR_MESSAGE].toString()

        if (metadata == NULL_STRING) metadata = event.properties["url"].toString()

        // If code is null or -1, report message instead
        if (StringUtil.isEmpty(code) || code == NULL_STRING || code == "-1") code = msg

        try {
            val error = event.properties[Event.ERROR]
            if (error is ExoPlaybackException) {
                val errorCodeMetadata = getErrorMetadata(error)
                if (error.type == ExoPlaybackException.TYPE_SOURCE) {
                    when (error.sourceException) {
                        is HttpDataSource.InvalidResponseCodeException -> invalidResponseCodeException(
                            error
                        )
                        is HttpDataSource.HttpDataSourceException -> httpDataSourceException(error)
                        is BehindLiveWindowException -> fireError(
                            error.errorCode.toString(),
                            error.message,
                            errorCodeMetadata
                        )
                        else -> fireFatalError(
                            error.errorCode.toString(),
                            error.message,
                            errorCodeMetadata
                        )
                    }
                } else {
                    fireFatalError(error.errorCode.toString(), error.message, errorCodeMetadata)
                }
                return
            }
        } catch (e: NoClassDefFoundError) {
            YouboraLog.debug("This is not an exoplayer compilation.")
        }

        if (code != "null" && msg != "null" && metadata != "null" && lastErrorCode != code) {
            when (event.type) {
                EventType.SOURCE_NOT_FOUND -> fireFatalError(msg, code, metadata)
                EventType.SOURCE_NOT_PLAYABLE -> fireFatalError(msg, code, metadata)
                else -> fireError(msg, code, metadata)
            }
        }
    }

    /** Exoplayer errors **/
    private fun getErrorClass(error: Exception): String? {
        return  error.cause?.javaClass?.name
    }
    private fun getErrorMetadata(error: ExoPlaybackException): String {
        return  error.errorCodeName + ": ${getErrorClass(error)}"
    }
    private fun invalidResponseCodeException(error: ExoPlaybackException ) {
        val invalidResponseCodeException =
            error.sourceException as HttpDataSource.InvalidResponseCodeException

        fireError(
            error.errorCode.toString(),
            error.message,
            "Response message: ${invalidResponseCodeException.responseMessage}, " + getErrorMetadata(error)
        )
    }


    private fun httpDataSourceException(error: ExoPlaybackException) {
        val httpDataSourceException =
            error.sourceException as HttpDataSource.HttpDataSourceException

        when (httpDataSourceException.type) {
            HttpDataSource.HttpDataSourceException.TYPE_OPEN -> fireFatalError(
                error.errorCode.toString(),
                "OPEN - ${error.message}",
                getErrorMetadata(error)
            )

            HttpDataSource.HttpDataSourceException.TYPE_READ -> fireFatalError(
                error.errorCode.toString(),
                "READ - ${error.message}",
                getErrorMetadata(error)
            )

            HttpDataSource.HttpDataSourceException.TYPE_CLOSE -> fireFatalError(
                error.errorCode.toString(),
                "CLOSE - ${error.message}",
                getErrorMetadata(error)
            )
        }
    }

    override fun getPlayerVersion(): String {
        var playerVersion = getPlayerName()

        if (lastReportedVersion != null) playerVersion += "-$lastReportedVersion"

        return playerVersion
    }

    override fun getPlayhead(): Double? {
        player.takeIf { !isAdBreak }?.let {
            lastReportedPlayhead = it.currentPositionLong.toDouble() / 1000
        }

        return lastReportedPlayhead
    }

    override fun getDuration(): Double? {
        return player?.durationLong?.takeIf { it > 0 }?.let { it / 1000.0 }
    }

    override fun getBitrate(): Long? {
        var bitrate: Long? = null

        lastReportedBitrate?.let { bitrate = it / 10 }

        return bitrate
    }

    override fun getPlayerName(): String { return "Brightcove" }
    override fun getTitle(): String? { return lastReportedTitle }
    override fun getResource(): String? { return lastReportedResource }
    override fun getRendition(): String? { return lastReportedRendition }
    override fun getIsLive(): Boolean? { return player?.videoDisplay?.isLive }
    override fun getVersion(): String { return BuildConfig.VERSION_NAME + "-" + getPlayerName() }

    private fun resetValues() {
        lastReportedResource = super.getResource()
        lastReportedBitrate = super.getBitrate()
        lastReportedTitle = super.getTitle()
        lastReportedRendition = super.getRendition()
        lastReportedPlayhead = super.getPlayhead()
        isAdBreak = false
    }
}
