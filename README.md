# Brightcove Player's Adapter Android

## Documentation, installation & usage

Add this to your build.gradle:

```groovy
repositories {
    ...
    maven { url  "https://npaw.jfrog.io/artifactory/youbora/" }
    ...
}

dependencies {
    ...
    implementation "com.nicepeopleatwork:brightcove-adapter:6.8.+"
    ...
}
```

Also please refer to [Developer Portal](http://developer.nicepeopleatwork.com).

## I need help!
If you find a bug, have a suggestion or need assistance, please send an e-mail to <support@nicepeopleatwork.com>.
