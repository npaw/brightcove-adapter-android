## [6.8.9] - 2024-10-10
### Update
- Update Lib version to `6.8.32`.

## [6.8.8] - 2024-07-18
### Update
- Update Lib version to `6.8.31`.

## [6.8.7] - 2024-07-04
### Update
- Update Lib version to `6.8.30`.

## [6.8.6] - 2023-05-15
### Update
- Update Lib version to `6.8.16`.
- Update Brightcove version to `8.1.0`.

## [6.8.5] - 2023-03-09
### Fixed
- Fixed AdPlayhead
- Handle Brightcove errors & Exoplayer errors
- Fixed Runtime error for none ssai compilations
- Fixed seek duration
- Fixed "no title" when app is coming from background
### Update
- Update Lib version to 6.8.9.
- Updated demos
### Added
- Added new demos: Exoplayer-IMA and BasicApp
- Added adDuration, adQuartiles, skippable

## [6.8.4] - 2022-09-27
### Fixed
- Correct Live content behavior firing multiple buffer events by adding tolerance of 150ms to any reported buffer.

## [6.8.3] - 2022-09-16
### Update
- Support Brightcove SDK from 7.0.1 to 7.1.1.

### [6.8.2] - 2022-09-16
### Update
- Update Lib version to 6.8.1.
### Fixed
- Ignored `onLoadErrors` while buffering.

## [6.8.1] - 2022-09-06
### Fixed
- Errors being ignored after retry playback.
- After resuming from an /adPause, /adStop is sent instead of /adResume.

## [6.8.0] - 2022-03-25
### Update
- Sdk Version Upgraded from 6.17.3 to 6.18.6.
- Upgrade minor version to work with the new lib version.
- Update Readme.
- Update Pipelines.
### Added
- Demo test for live content.
- Additional logs. 

## [6.7.2] - 2022-03-25
### Update
- Sdk Version Upgraded.
### Removed
- Removed reference to outdated Balancer Lib.

## [6.7.1] - 2021-09-15
### Added
- willChangeVideo open function.
### Modified
- Deployment platform moved from Bintray to JFrog.

## [6.7.0] - 2020-03-05
### Refactored
- Adapter to work with the new Youboralib version.
### Removed 
- GenericAdsAdapter.

## [6.6.0] - 2020-01-15
### Update
- Youboralib version.

## [6.5.1] - 2019-11-20
### Fixed
- Last release not published correctly.

## [6.5.0] - 2019-08-05
### Added
- Ads rework.
- Moved to Kotlin.
### Fixed
- adPlayhead reported as adBreakPlayhead.
### Deprecated
- GenericAdsAdapter.

## [6.4.4] - 2019-07-26
### Fixed
- Bitrate not reported correctly.

## [6.4.3] - 2019-07-17
### Fixed
- adStop sent when resuming an ad.

## [6.4.2] - 2019-07-12
### Fixed
- playerVersion and adAdapterVersion.

## [6.4.1] - 2019-05-10
### Fixed
- Duplicated ads when coming from the background.

## [6.4.0] - 2019-04-26
### Updated
- Youboralib upgraded to version 6.4.2.

## [6.3.0] - 2019-02-11
### Updated
- Youboralib upgraded to version 6.3.6.

## [6.0.4] - 2018-12-06
### Fixed
- Seek may be reported as buffer.

## [6.0.3] - 2018-08-06
### Added
- Ads support.

## [6.0.2] - 2018-02-16
### Added
- Youboralib upgraded to version 6.1.0.
  
## [6.0.2-beta] - 2018-01-31
### Fixed
- Multiple errors being reported.
 
## [6.0.1-beta] - 2018-01-31
### Added
- Youboralib upgraded to version 6.1.0-beta2.
 
## [6.0.0] - 2017-11-16
### Added
- First release.